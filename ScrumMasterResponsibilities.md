## Responsibilities of a scrum master

* * Scrum Master is responsible for making sure teams correctly follow the rules and principles that govern scrum.
* * Always be focused and goal oriented.
* * Be a coach to both development team and the product owner