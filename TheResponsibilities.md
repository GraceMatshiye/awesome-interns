#The Responsibilities of a Scrum Master

##Responsibilities of a Scrum Master
1. Coach
2. Servant Leader
3. Process authority
4. Interference shield
5.Impediment remover
6. Change agent