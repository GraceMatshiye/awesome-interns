#Scrum Master Roles and responsibilities

##Roles of a Scrum Master
1. Facilitator basically deliver maximum value to the customer.
2. Focused and Orientated  responsible for keeping the goals clear and visible to the team.
3. Leadership and finding effective ways to communicate project visions  .


##Responsibilities of a Scrum Master
1. Coach
2. Servant Leader
3. Process authority
4. Interference shield
5. Impediment remover
6. Change agent
